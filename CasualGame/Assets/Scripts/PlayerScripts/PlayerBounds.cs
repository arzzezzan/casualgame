using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBounds : MonoBehaviour
{
    public float minX = -2.20f, maxX = 2.20f, minY = -5.9f;
    private bool outOfBounds;
    void Update()
    {
        CheckBounds();
    }
    void CheckBounds()
    {
        Vector2 temp = transform.position;
        if(temp.x > maxX)
        {
            temp.x = maxX;
        }
        if (temp.x < minX)
        {
            temp.x = minX;
        }
        transform.position = temp;
        if(temp.y <= minY)
        {
            Destroy(gameObject);
            SoundManager.instance.DeathSound();
            GameManager.instance.RestartGame();
        }

    }
    private void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Spikes")
        {
            transform.position = new Vector2(1000f, 1000f);
            SoundManager.instance.DeathSound();
            GameManager.instance.RestartGame();
        }
    }
}
