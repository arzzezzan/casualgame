using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D myBody;

    private float moveSpeed;
    public float currentSpeed;

    void Awake()
    {
        moveSpeed = 0f;
        myBody = GetComponent<Rigidbody2D>();
    }
   

    void FixedUpdate()
    {
        myBody.velocity = new Vector2(moveSpeed, myBody.velocity.y);
    }
    public void MoveLeft()
    {
        if(moveSpeed >= 0f)
        {
            moveSpeed = -currentSpeed;
        }
    }
    public void MoveRight ()
    {
        if (moveSpeed <= 0f)
        {
            moveSpeed = currentSpeed;

        }
    }
    public void StopMove()
    {
        moveSpeed = 0;
    }
    public void PlatformMove(float x)
    {
       myBody.velocity = new Vector2(x, myBody.velocity.y);
    }
}

