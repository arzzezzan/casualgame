using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    [SerializeField]
    private AudioSource m_AudioSource;
    [SerializeField]
    private AudioClip landClip, deathClip, iceBreakClip, gameOverClip;



    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void LandSound()
    {
        m_AudioSource.clip = landClip;
        m_AudioSource.Play();
    }
    public void IceBreakSound()
    {
        m_AudioSource.clip = iceBreakClip;
        m_AudioSource.Play();
    }
    public void DeathSound()
    {
        m_AudioSource.clip = deathClip;
        m_AudioSource.Play();
    }
    public void GameOverSound()
    {
        m_AudioSource.clip = gameOverClip;
        m_AudioSource.Play();
    }

}
