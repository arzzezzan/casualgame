using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    public GameObject platformPrefab;
    public GameObject spikePlarformPrefab;
    public GameObject[] movingPlatformsPrefab;
    public GameObject breakablePLatformPrefab;

    public float spawnerTimer = 2f;
    private float currentSpawnerTimer;
    
    private int SpawnerCount;

    public float minX = -2.20f, maxX = 2.20f;
    void Start()
    {
        currentSpawnerTimer = spawnerTimer;
    }

    // Update is called once per frame
    void Update()
    {
        SpawnPlatforms();
    }
    void SpawnPlatforms()
    {
        currentSpawnerTimer += Time.deltaTime;
        if(currentSpawnerTimer >= spawnerTimer)
        {
            SpawnerCount++;
            Vector3 temp = transform.position;
            temp.x = Random.Range(minX, maxX);

            GameObject newPlatform = null;
            if(SpawnerCount < 2)
            {
                newPlatform = Instantiate(platformPrefab, temp, Quaternion.identity);
            }
            else if(SpawnerCount == 2)
            {
                if(Random.Range(0, 2) > 0)
                {
                    newPlatform = Instantiate(platformPrefab, temp, Quaternion.identity);
                }
                else
                {
                    newPlatform = Instantiate(movingPlatformsPrefab[Random.Range(0, movingPlatformsPrefab.Length)], temp, Quaternion.identity);
                }
            }
            else if (SpawnerCount == 3)
            {
                if (Random.Range(0, 2) > 0)
                {
                    newPlatform = Instantiate(platformPrefab, temp, Quaternion.identity);
                }
                else
                {
                    newPlatform = Instantiate(spikePlarformPrefab, temp, Quaternion.identity);
                }




            }
            else if (SpawnerCount == 4)
            {
                if (Random.Range(0, 2) > 0)
                {
                    newPlatform = Instantiate(platformPrefab, temp, Quaternion.identity);
                }
                else
                {
                    newPlatform = Instantiate(breakablePLatformPrefab, temp, Quaternion.identity);
                }


                SpawnerCount = 0;

            }
            newPlatform.transform.parent = transform;
            currentSpawnerTimer = 0f;


        }
    }
}
