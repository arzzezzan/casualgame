using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgScroll : MonoBehaviour
{
    public float scroll_Speed = 0.3f;

    private MeshRenderer m_MeshRenderer;
    void Awake()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();
    }
    void Update()
    {
        Scroll();
    }
    void Scroll()
    {
        Vector2 offset = m_MeshRenderer.sharedMaterial.GetTextureOffset("_MainTex");
        offset.y += Time.deltaTime * scroll_Speed;
        m_MeshRenderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}
